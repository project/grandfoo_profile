core = 7.0

; Themes
projects[mayo] = "1.1"

; default modules
projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "2.1"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.0-beta1"

projects[imce][subdir] = "contrib"
projects[imce][version] = "1.3"

;projects[imce_wysiwyg][subdir] = "contrib"
;projects[imce_wysiwyg][version] = "1.x-dev"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-beta2"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "1.0-beta4"

projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.4-beta1"

projects[insert][subdir] = "contrib"
projects[insert][version] = "1.0"

projects[token][subdir] = "contrib"
projects[token][version] = "1.0-beta1"

projects[captcha-free][subdir] = "contrib"
projects[captcha-free][version] = "1.0-alpha1"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.0-beta1"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.2"
